source_code_path = ./src/
main: $(source_code_path)POOBBS.java $(source_code_path)POOArticle.java $(source_code_path)POOBoard.java $(source_code_path)POODirectory.java $(source_code_path)POOEntry.java $(source_code_path)POOUser.java
	javac $(source_code_path)POOBBS.java $(source_code_path)POOArticle.java $(source_code_path)POOBoard.java $(source_code_path)POODirectory.java $(source_code_path)POOEntry.java $(source_code_path)POOUser.java
run: $(source_code_path)POOBBS.java
	make
	java -classpath $(source_code_path) POOBBS
clean: $(source_code_path)POOBBS.class $(source_code_path)POOArticle.class $(source_code_path)POOBoard.class $(source_code_path)POODirectory.class $(source_code_path)POOEntry.class $(source_code_path)POOUser.class
	rm $(source_code_path)*.class
	

