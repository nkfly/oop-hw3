import java.lang.*;
import java.io.*;
import java.util.*;

public class POOBBS{
	
	public static final String indexPath = "./0_index_Directory";
	public static final int depthMax = 32;


	
	public static void main(String [] argv){

		System.out.println("The one-user POOBBS is now launching");


		POOEntry currentFrame = new POODirectory(indexPath);
		Scanner askUser = new Scanner(System.in);

		

		System.out.println("Please enter your user name");
		POOUser user = new POOUser(askUser.nextLine());

		System.out.println("Welcome, " + user.getName());


		POOEntry [] traverse = new POOEntry[depthMax];
		int traverseIndex = 0;
		traverse[traverseIndex++] = currentFrame;

		String commandLine = null;
		String [] commands;
		

		while(true){

			currentFrame.show();

			System.out.println("Please input your command. For any help, try 'help'");

			commandLine = askUser.nextLine();
			commands = commandLine.split(" ");

			if(commands.length == 2 && commands[0].equals("visit")){
				int res = Integer.parseInt(commands[1]);
				if(res < 0 || res > currentFrame.length()){
					System.out.println("wrong input number");
				}else if(res == currentFrame.length()){
					if(traverseIndex-2 >= 0){
						currentFrame = traverse[traverseIndex-2];
						traverseIndex--;
					}else return;	
				}else {
					POOEntry nextFrame= currentFrame.visit(res);
					if(currentFrame != nextFrame){
						traverse[traverseIndex++] = nextFrame;
						currentFrame = nextFrame;
					}
				}
			}else {
				currentFrame.executeCommand(user , commandLine);
			}

			
			
			
		

	}

}

}