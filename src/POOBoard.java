import java.io.*;
import java.awt.*;
import java.util.*;

public class POOBoard extends POOEntry{
	private static final int MAX_ARTICLE_NUM = 1024;
	//private static final int ATTRIBUTE_NUM = 2;

	private String [] articles = null;
	

	public POOBoard(String path){
		String [] temp;

		this.path = path;
		temp = path.split("/");
		this.title = temp[temp.length-1].split("_")[1];


		File currentFile = new File(this.path);
		articles = new String[MAX_ARTICLE_NUM];

		if(currentFile.isDirectory()){
			temp = currentFile.list();
			length = temp.length;
			if(length <= MAX_ARTICLE_NUM ){
				for(String s : temp){
					articles[Integer.parseInt(s.split("_")[0])] = s;
				}

			}
		}

	}
	public void showFunction(){
		System.out.println("****************************Board Administration Function****************************");
		System.out.println("To append an article, command 'add  article [article title] [article content in a line]'. ex: 'add article CSIE The content starts here until line break");
		System.out.println("To delete an article, command 'delete [position number]'. ex: 'delete 0'");
		System.out.println("To move the article, command 'move [source number] [destination number]'. ex: 'move 0 3'");
		System.out.println("****************************Article Operation Function****************************");
		System.out.println("To read an article, command 'show [article number]'. ex: 'show 0'");
		System.out.println("To push an article, command 'push [article number] [message]'. ex: 'push 0 I like this article'");
		System.out.println("To boo an article, command 'boo [article number] [message]'. ex: 'boo 0 This comment is bad'");
		System.out.println("To arrow an article, command 'arrow [article number] [message]'. ex: 'arrow 0 Extra opinion is here'");
		System.out.println("To list the evaluation count, ID, title, and author of an article, command 'list [article number]'. ex: 'list 0'");


	}

	public void executeCommand(POOUser user, String  commandLine){
		String [] commands = commandLine.split(" ");
		if(commands.length == 1 && commands[0].equals("help")){
			showFunction();
		}else if(commands.length >= 4 && commands[0].equals("add") && commands[1].equals("article") ){
			add(length + "_" + commands[2] + "_" + user.getName(), commandLine.substring(commandLine.indexOf(commands[3]))  );
		}else if(commands.length == 2 && commands[0].equals("delete") && Integer.parseInt(commands[1]) >= 0 && Integer.parseInt(commands[1]) < length){
			delete(Integer.parseInt(commands[1]));
		}else if(commands.length == 3 && commands[0].equals("move") ){
			move(Integer.parseInt(commands[1]), Integer.parseInt(commands[2]) );
		}else if(commands.length == 2 && commands[0].equals("show") && Integer.parseInt(commands[1]) >= 0 && Integer.parseInt(commands[1]) < length){
			new POOArticle(path + "/" + articles[Integer.parseInt(commands[1])], user).show();
		}else if(commands.length >=  3 && commands[0].equals("push") && Integer.parseInt(commands[1]) >= 0 && Integer.parseInt(commands[1]) < length){
			new POOArticle(path + "/" + articles[Integer.parseInt(commands[1])], user).push(commandLine.substring(commandLine.indexOf(commands[2])));
		}else if(commands.length >=  3 && commands[0].equals("boo") && Integer.parseInt(commands[1]) >= 0 && Integer.parseInt(commands[1]) < length){
			new POOArticle(path + "/" + articles[Integer.parseInt(commands[1])], user).boo(commandLine.substring(commandLine.indexOf(commands[2])));
		}else if(commands.length >=  3 && commands[0].equals("arrow") && Integer.parseInt(commands[1]) >= 0 && Integer.parseInt(commands[1]) < length){
			new POOArticle(path + "/" + articles[Integer.parseInt(commands[1])], user).arrow(commandLine.substring(commandLine.indexOf(commands[2])));
		}else if(commands.length ==  2 && commands[0].equals("list") && Integer.parseInt(commands[1]) >= 0 && Integer.parseInt(commands[1]) < length){
			new POOArticle(path + "/" + articles[Integer.parseInt(commands[1])], user).list();
		}else {
			System.out.println("unrecongnized command");
		}
		


	}

	public void add(String articleName, String content){
		
		if(length + 1 >= MAX_ARTICLE_NUM){
			System.out.println("The number of articles has reached the maximum!");
			return;
		}

		try{
  		// Create file 
  		FileWriter fstream = new FileWriter(path + "/" + articleName);
  		BufferedWriter out = new BufferedWriter(fstream);

  		

		out.write(content);
  		
  		//Close the output stream
  		out.close();
  		}catch (Exception e){//Catch exception if any
  		System.err.println("Error: " + e.getMessage());
  		}
		articles[length++] = articleName;



	}



	public void delete(int pos){
		if(pos < 0 || pos >= length){
			System.out.println("invalid position");
			return;

		}
		String fileName = articles[pos];
		System.out.printf("Are you sure you want to delete %s ? (y/n)", fileName.split("_")[1]);
		String answer = new Scanner(System.in).nextLine();
		if(answer.contains("N") || answer.contains("n"))return;

		File f = new File(path + "/" + fileName);



		boolean success = f.delete();

		if (success){
			for(int i = pos+1 ;i < length; i++ ){
				String [] splitTemp = articles[i].split("_");
				String newName = (i-1) + "_" + splitTemp[1] + "_" + splitTemp[2];
				File oldFile = new File(path + "/" + articles[i]);
				File newFile = new File(path + "/" + newName);
				success = oldFile.renameTo(newFile);
				if (success) {
    				articles[i-1] = newName;
				}else System.out.println("fail to rename");


			}
			length--;

		}


	}

	public void move(int src, int des){
		if(src < 0 || src >= length){
			System.out.println("The source number is wrong");
			return;
		}
		int destination;
		if(des < 0)destination = 0;
		else if(des >= length)destination = length-1;
		else destination = des;

		String sourceName = articles[src];
		boolean success;

		String [] splitSource = sourceName.split("_");
		File oldFile = new File(path + "/" + sourceName);
		File newFile = new File(path + "/" + length+ "_" + splitSource[1] + "_" + splitSource[2]);
		success = oldFile.renameTo(newFile);



		if(src <= destination){
			for(int i = src + 1; i <= destination;i++){
				String [] splitTemp = articles[i].split("_");
				String newName = (i-1) + "_" + splitTemp[1] + "_" + splitTemp[2];
				oldFile = new File(path + "/" + articles[i]);
				newFile = new File(path + "/" + newName);
				success = oldFile.renameTo(newFile);
				if (success) {
    				articles[i-1] = newName;
				}else System.out.println("fail to rename");

			}
			
			String newName = destination+ "_" + splitSource[1] + "_" + splitSource[2];
			oldFile = new File(path + "/" + length + "_" + splitSource[1] + "_" + splitSource[2]);
			newFile = new File(path + "/" + newName);
			success = oldFile.renameTo(newFile);
			if (success) {
    				articles[destination] = newName;
				}else System.out.println("fail to rename");

		}else {
			for(int i = src -1; i  >= destination;i--){
				String [] splitTemp = articles[i].split("_");
				String newName = (i+1) + "_" + splitTemp[1] + "_" + splitTemp[2];
				oldFile = new File(path + "/" + articles[i]);
				newFile = new File(path + "/" + newName);
				success = oldFile.renameTo(newFile);
				if (success) {
    				articles[i+1] = newName;
				}else System.out.println("fail to rename");

			}
			
			String newName = destination+ "_" + splitSource[1] + "_" + splitSource[2];
			oldFile = new File(path + "/" + length + "_" + splitSource[1] + "_" + splitSource[2]);
			newFile = new File(path + "/" + newName);
			success = oldFile.renameTo(newFile);
			if (success) {
    			articles[destination] = newName;
			}else System.out.println("fail to rename");


		}


	}

	
	
	protected POOEntry visit(int number){
		return this;

	}

	public void show(){

		System.out.println("");
		System.out.println("Board:" + title);
		
		for(int i = 0;i < length;++i){
			if(articles[i] != null){
				String [] temp = articles[i].split("_");
				System.out.printf("(%d) Article %s:%s\n",i,  temp[2], temp[1]);
			}
			
		}

		System.out.printf("(%d) Back\n", length);
		System.out.println("");
	}


}