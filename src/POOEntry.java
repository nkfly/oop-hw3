public abstract class POOEntry{

	protected String title = null;
	protected String path = null;
	protected int length = 0;



	

	public abstract void show();

	public abstract void executeCommand(POOUser user, String commandLine);

	public int length(){
		return length;
	}

	protected abstract POOEntry visit(int number);



	

}