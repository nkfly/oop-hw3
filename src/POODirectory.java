import java.io.*;
import java.util.*;

public class POODirectory extends POOEntry{

	private static final int MAX_ENTRY_NUM = 1024;

	private File currentFile = null;
	private String [] entries = null;
	
	
	public POODirectory(String path){

		String [] temp;

		this.path = path;
		temp = path.split("/");
		this.title = temp[temp.length-1].split("_")[1];


		currentFile = new File(this.path);
		entries = new String[MAX_ENTRY_NUM];

		if(currentFile.isDirectory()){
			temp = currentFile.list();
			length = temp.length;

			if(length <= MAX_ENTRY_NUM ){

				for(String s : temp){
					entries[Integer.parseInt(s.split("_")[0])] = s;
				}

			}
		}

	}

	
	public void show(){
		System.out.println("");
		System.out.println("Directory:" + title);
		String [] temp = null;
				
		for(int i = 0;i < length;++i){
			if(entries[i] != null){
				temp = entries[i].split("_");
				if(temp[2].equals("Split")){
					System.out.printf("(%d) ----------------------------------------\n", i);
				}else System.out.printf("(%d) %s %s\n",i, temp[2], temp[1] );

			}

			
		}

		System.out.printf("(%d) Back\n", length);
		System.out.println("");
		


		


	}

	public void add_directory(String directoryName){
		new File(path + "/" + directoryName).mkdir();
		entries[length++] = directoryName;

	}

	public void add_board(String boardName){
		new File(path + "/" + boardName).mkdir();
		entries[length++] = boardName;

	}

	public void add_split(){
		String splitName = length + "_x_Split";
		try {
    		new File(path + "/" + splitName).createNewFile();
		} catch (IOException e) {
    		e.printStackTrace();
	    }
		entries[length++] = splitName;

	}

	public void delete(int pos){
		String fileName = entries[pos];

		System.out.printf("Are you sure you want to delete %s ? (y / n)", fileName.split("_")[1]);
		String answer = new Scanner(System.in).nextLine();
		if(answer.contains("N") || answer.contains("n"))return;

		File f = new File(path + "/" + fileName);


		if (f.isDirectory() && f.list().length > 0) {
		  System.out.println("The entry is not empty, cannot be deleted");
		}

		boolean success = f.delete();

		if (success){
			for(int i = pos+1 ;i < length; i++ ){
				String [] splitTemp = entries[i].split("_");
				String newName = (i-1) + "_" + splitTemp[1] + "_" + splitTemp[2];
				File oldFile = new File(path + "/" + entries[i]);
				File newFile = new File(path + "/" + newName);
				success = oldFile.renameTo(newFile);
				if (success) {
    				entries[i-1] = newName;
				}else System.out.println("fail to rename");


			}
			length--;

		}else {
			System.out.println("The file cannot be deleted");
		}

	}

	public void move(int src, int des){
		if(src < 0 || src >= length){
			System.out.println("The source number is wrong");
			return;
		}
		int destination;
		if(des < 0)destination = 0;
		else if(des >= length)destination = length-1;
		else destination = des;

		String sourceName = entries[src];
		String [] splitSource = sourceName.split("_");
		File oldFile = new File(path + "/" + sourceName);
		File newFile = new File(path + "/" + length+ "_" + splitSource[1] + "_" + splitSource[2]);

		boolean success;
		success = oldFile.renameTo(newFile);



		if(src <= destination){
			for(int i = src + 1; i <= destination;i++){
				String [] splitTemp = entries[i].split("_");
				String newName = (i-1) + "_" + splitTemp[1] + "_" + splitTemp[2];
				oldFile = new File(path + "/" + entries[i]);
				newFile = new File(path + "/" + newName);
				success = oldFile.renameTo(newFile);
				if (success) {
    				entries[i-1] = newName;
				}else System.out.println("fail to rename");

			}
			
			String newName = destination+ "_" + splitSource[1] + "_" + splitSource[2];
			oldFile = new File(path + "/" + length + "_" + splitSource[1] + "_" + splitSource[2]);
			newFile = new File(path + "/" + newName);
			success = oldFile.renameTo(newFile);
			if (success) {
    				entries[destination] = newName;
				}else System.out.println("fail to rename");

		}else {
			for(int i = src -1; i  >= destination;i--){
				String [] splitTemp = entries[i].split("_");
				String newName = (i+1) + "_" + splitTemp[1] + "_" + splitTemp[2];
				oldFile = new File(path + "/" + entries[i]);
				newFile = new File(path + "/" + newName);
				success = oldFile.renameTo(newFile);
				if (success) {
    				entries[i+1] = newName;
				}else System.out.println("fail to rename");

			}
			
			String newName = destination+ "_" + splitSource[1] + "_" + splitSource[2];
			oldFile = new File(path + "/" + length + "_" + splitSource[1] + "_" + splitSource[2]);
			newFile = new File(path + "/" + newName);
			success = oldFile.renameTo(newFile);
			if (success) {
    			entries[destination] = newName;
			}else System.out.println("fail to rename");


		}


		


	}

	public void executeCommand(POOUser user, String commandLine){
		String [] commands = commandLine.split(" ");
		if(commands.length == 1 && commands[0].equals("help")){
			showFunction();
		}else if(commands.length >= 3 && commands[0].equals("add") && commands[1].equals("directory") ){
			add_directory(length + "_" + commandLine.substring(commandLine.indexOf(commands[2])) + "_Directory");
		}else if(commands.length >= 3 && commands[0].equals("add") && commands[1].equals("board") ){
			add_board(length + "_" + commandLine.substring(commandLine.indexOf(commands[2])) + "_Board");
		}else if(commands.length == 2 && commands[0].equals("add") && commands[1].equals("split") ){
			add_split();
		}else if(commands.length == 2 && commands[0].equals("delete") ){
			delete(Integer.parseInt(commands[1]));
			
		}else if(commands.length == 3 && commands[0].equals("move") ){
			move(Integer.parseInt(commands[1]), Integer.parseInt(commands[2]) );
		}
		


	}

	private void showFunction(){
		System.out.println("To visit a board/directory, command 'visit [position number]'. ex: 'visit 0'");
		System.out.println("To append a directory, command 'add directory [directory title]'. ex: 'add directory CSIE'");
		System.out.println("To append a board, command 'add board [board title]'. ex: 'add board OOP'");
		System.out.println("To append a split line, command 'add split'. ex: 'add split'");
		System.out.println("To delete a board/directory/splitting line, command 'delete [position number]'. ex: 'delete 0'");
		System.out.println("To move the board/directory/splitting line, command 'move [source number] [destination number]'. ex: 'move 0 3'");

	}

	public POOEntry visit(int number){
		if(number < 0 || number >= length || entries[number].equals(number + "_x_Split")){
			System.out.println("Sorry, you can not visit this entry");
			return this;
		}

		String nextPath = "/" + entries[number];


		String [] splitedAttribute = entries[number].split("_");

		if(splitedAttribute[2].equals("Directory")){
			return new POODirectory(this.path + nextPath);

		}else if(splitedAttribute[2].equals("Board")){
			return new POOBoard(this.path + nextPath);

		}
		return null;



	}
	

	

}